
var navbar_toggle = document.querySelector(".navbar-toggler");
// console.log(navbar_toggle);

var site_header = document.getElementById('site-header');
navbar_toggle.addEventListener('click', () => {
    site_header.classList.toggle('active')
});


// slick
$('.banner__slider').slick({
    dots: false,
    infinite: true,
    // speed: 500,
    fade: true,
    cssEase: 'linear',
    auto: true,
    autoplay: 200,
});

document.querySelector('.slick-next').innerHTML = `<i class="fa-solid fa-arrow-left"></i>`
document.querySelector('.slick-prev').innerHTML = `<i class="fa-solid fa-arrow-right"></i>`
// sticky
$(document).ready(function () {
    $(window).scroll(function () {
        if (this.scrollY > 30) {
            $('.main-header').addClass('sticky');
        } else {
            $('.main-header').removeClass('sticky');

        }

    })
})

// click search
var body = document.getElementById('body')
document.querySelector('.control').addEventListener('click', () => {
    body.classList.add("search__active");
    // console.log(e.target);
});

document.querySelector('.icon-close').addEventListener('click', () => {
    $('.body').removeClass('search__active');

});
// owlcarousel
$('.owl-carousel').owlCarousel({
    rtl:true,
    loop:true,
    margin:10,
    nav:true,
    responsive:{
        0:{
            items:1
        },

    }
})
rlt: true;
// Go top
var mybtn = document.getElementById("movetop");
window.onscroll = function() {topFunction};
function topFunction(){
document.body.scrollTop = 0;
document.documentElement.scrollTop = 0;
}